# A Site redesign idea for Andy

## The Idea
Andy Prickett maintains a Wordpress based site for himself. He is also a
skilled web developer. In honor of his nearly 4 year journey, I have created
a single page version of his existing site - simplifying it, of course - down
to a literal single page. Seriously, it is just a single `index.html` file with
some CSS and images.

Of course, it maintains usability on both mobile and laptap formats. This is really
just an example page I use for some of my own students who are just beginning their
journey. And it is an honor to make it in honor of Andy.

## Usage
1. Clone the repository
```
git clone https://gitlab.com/gginorio/andy.git
cd andy
```
2. Open the `index.html` file with your browser.
3. You can see it live here: https://gginorio.gitlab.io/andy/

## It should look like this

![Screenshot](public/assets/images/screenshot.png "Screenshot")